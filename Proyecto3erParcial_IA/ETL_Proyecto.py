import ProyectoF_AIID


def etl_report(etl, parameters):
    etl.extract()
    etl.transform()
    etl.load()

def main():
    print("Starting")
    # Parameters
    parameters = [
        'estatus-socioeconomico',
        'estatus-socioeconomico'
    ]

    # Init
    etl_obj = ProyectoF_AIID.ETL(parameters[0], parameters[1])

    # run application
    etl_report(etl_obj, parameters)

main()