import boto3
import pandas as pd
from io import StringIO, BytesIO
#from datetime import datetime, timedelta, time
#import pyarrow as pa
#import pyarrow.parquet as pq


class ETL():
    def __init__(self, bucket_name, report_bucket_name):
        self.s3 = boto3.client('s3')
        self.bucket_name = bucket_name
        self.report_bucket_name = report_bucket_name
        self.data = None

    def extract(self):
        print("Extracting 1")
        dfs = []

        objects = self.s3.list_objects_v2(Bucket=self.bucket_name)

        for obj in objects['Contents']:
            key = obj['Key']
            if key.endswith('.csv') and key.startswith('conjunto_de_datos_viviendas_enigh_') and '_ns.csv' in key:
                obj = self.s3.get_object(Bucket=self.bucket_name, Key=key)
                df = pd.read_csv(BytesIO(obj['Body'].read()))
                dfs.append(df)
        self.data = pd.concat(dfs, ignore_index=True)
        print(self.data) #Donde se guarda todo el conjunto

    def transform(self):
       print("Tranforming 1")
       #Llenar los campos vacios con 0
       self.data = self.data.replace(' ', pd.NA)
       self.data = self.data.fillna(0)
       self.data = self.data.replace('&', 0)
       print(self.data)

       #Eliminacion de columnas inecesarias
       self.data = self.data.drop(columns=['folioviv', 'antiguedad', 'antigua_ne', 'cocina_dor',
                                           'uso_compar', 'biodigest', 'estim_pago',
                                           'pago_viv', 'pago_mesp', 'tipo_adqui',
                                           'viv_usada', 'tipo_finan', 'num_dueno1',
                                           'hog_dueno1', 'num_dueno2', 'hog_dueno2',
                                           'tot_hog', 'ubica_geo', 'tam_loc',
                                           'est_socio', 'est_dis', 'upm', 'factor', 'procaptar'])
       print(self.data)

    def load(self):
        print("Load")
        #Convertir el DataFrame a CSV en memoria
        csv_buffer = StringIO()
        self.data.to_csv(csv_buffer, index=False)

        #Subir el archivo CSV al bucket de S3
        self.s3.put_object(Bucket=self.report_bucket_name, Key='output.csv', Body=csv_buffer.getvalue())
