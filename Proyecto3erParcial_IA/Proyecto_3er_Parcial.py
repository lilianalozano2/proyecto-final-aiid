import time
import pandas as pd
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


start_time = time.time()

# 1- We load our dataset
data = pd.read_csv('Dataset_viviendas.csv')

data_columns = data[['mat_pared', 'mat_techos', 'mat_pisos', 'tenencia']]
Order_columns = data_columns.apply(lambda x: x.sort_values().values)

# 2- We normalize the data
scaler = MinMaxScaler()
data_normalized = scaler.fit_transform(data)

# 3- Now we use the elbow method to find the appropriate number of clusters.
inertia = []
for k in range(1, 11):
    kmeans = KMeans(n_clusters=k, random_state=42)
    kmeans.fit(data_normalized)
    inertia.append(kmeans.inertia_)

plt.plot(range(1, 11), inertia, marker='o')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.show()

# 4- Once the graph has been analyzed, we establish the number of clusters
# and prepare and train the K-means model.
kmeans = KMeans(n_clusters=5, random_state=42)
kmeans.fit(data_normalized)

# 5- Now add the column cluster to dataframe and print the clusters
data['Cluster'] = kmeans.labels_
print(data)


# From here we were able to make an interpretation of the results obtained

cluster_means = data.groupby('Cluster').mean()
plt.figure(figsize=(10, 12))
sns.heatmap(cluster_means.T, annot=True, cmap='viridis')
plt.title('Medias de las características por cluster')
plt.xlabel('Cluster')
plt.ylabel('Características')
plt.show()



scaler = StandardScaler()
data_scaled = scaler.fit_transform(data)
pca = PCA(n_components=2)
data_pca = pca.fit_transform(data_scaled)

custom_palette = plt.get_cmap('viridis', 5)
fig, axs = plt.subplots(nrows=1, ncols=5, figsize=(20, 5))

for i, ax in enumerate(fig.axes, start=1):
    kmeans = KMeans(n_clusters=i, random_state=42)
    clusters = kmeans.fit_predict(data_pca)
    ax.scatter(x=data_pca[:, 0], y=data_pca[:, 1], c=clusters, cmap=custom_palette)
    ax.set_title(f'N Clusters: {i}')
    ax.set_xlabel('Principal Component 1')
    ax.set_ylabel('Principal Component2')

plt.tight_layout()
plt.show()


end_time = time.time()
execution_time = end_time - start_time

print(f"Execution time {execution_time}.")

