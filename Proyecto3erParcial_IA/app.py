from flask import Flask, render_template
import os

app = Flask(__name__)


@app.route('/')
def index():
    elbow_path = 'static/plots/elbow_method.png'
    heatmap_path = 'static/plots/heatmap.png'
    scatter_plots_path = []

    for i in range(1, 6):
        scatter_plot_path = f'static/plots/scatter_plot_{i}.png'
        scatter_plots_path.append(scatter_plot_path)

    return render_template('index.html', elbow_path=elbow_path, heatmap_path=heatmap_path,
                           scatter_plots_path=scatter_plots_path)


if __name__ == '__main__':
    app.run(debug=True)
