import pandas as pd
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler, StandardScaler
import matplotlib.pyplot as plt
import os

# Crear el directorio para los gráficos si no existe
if not os.path.exists('static/plots'):
    os.makedirs('static/plots')

# 1- We load our dataset
data = pd.read_csv('Dataset_viviendas.csv')

data_columns = data[['mat_pared', 'mat_techos', 'mat_pisos', 'tenencia']]
Order_columns = data_columns.apply(lambda x: x.sort_values().values)

# 2- We normalize the data
scaler = MinMaxScaler()
data_normalized = scaler.fit_transform(data)

# 3- Now we use the elbow method to find the appropriate number of clusters.
inertia = []
for k in range(1, 11):
    kmeans = KMeans(n_clusters=k, random_state=42)
    kmeans.fit(data_normalized)
    inertia.append(kmeans.inertia_)

plt.figure()
plt.plot(range(1, 11), inertia, marker='o')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.title('Elbow Method')
plt.savefig('static/plots/elbow_method.png')
plt.close()

# 4- Once the graph has been analyzed, we establish the number of clusters
# and prepare and train the K-means model.
kmeans = KMeans(n_clusters=5, random_state=42)
kmeans.fit(data_normalized)

# 5- Now add the column cluster to dataframe and print the clusters

data['Cluster'] = kmeans.labels_
print(data.groupby('Cluster').mean())

data['Cluster'] = kmeans.labels_
cluster_means = data.groupby('Cluster').mean()
heatmap_path = 'static/plots/heatmap.png'
plt.figure(figsize=(10, 10))
sns.heatmap(cluster_means.T, annot=True, cmap='viridis')
plt.title('Cluster mean characteristics')
plt.xlabel('Cluster')
plt.ylabel('characteristics')
plt.savefig(heatmap_path)

scaler = StandardScaler()
data_scaled = scaler.fit_transform(data)
pca = PCA(n_components=2)
data_pca = pca.fit_transform(data_scaled)

custom_palette = plt.get_cmap('viridis', 5)
scatter_plots_path = []
for i in range(1, 6):
    plt.figure(figsize=(6, 6))
    kmeans = KMeans(n_clusters=i, random_state=42)
    clusters = kmeans.fit_predict(data_pca)
    plt.scatter(x=data_pca[:, 0], y=data_pca[:, 1], c=clusters, cmap=custom_palette)
    plt.title(f'N Clusters: {i}')
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component2')
    scatter_plot_path = f'static/plots/scatter_plot_{i}.png'
    plt.savefig(scatter_plot_path)
    scatter_plots_path.append(scatter_plot_path)
